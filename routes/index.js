var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/callbacktest', function(req, res, next) {
  console.log("======================================")
  console.log(new Date())
  console.log(req.query)
  console.log("======================================")
  res.status(200).json({success: true})
});

module.exports = router;
